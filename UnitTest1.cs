using System;
using Xunit;

namespace example
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(1, 1);
        }

        [Theory]
        [InlineData("sentence", "Sentence")]
        [InlineData("a", "A")]
        [InlineData("", "")]
        public void CapitalizationTest(string input, string expected)
        {
            var result = SomeCode.Capitalize(input);
            Assert.Equal(expected, result);
        }
    }
}
