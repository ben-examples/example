namespace example
{
    public static class SomeCode
    {
        public static string Capitalize(string sentence) {
            string first() => sentence[0].ToString().ToUpper();
            string rest() => sentence.Substring(1);
            return sentence switch 
            {
                var empty when empty.Length == 0 => "",
                var single when single.Length == 1 => single.ToUpper(),
                var normal => first() + rest()
            };
        }
    }
}